## REDGPU Framework

```c
// REDGPU Framework "Hello, World!" Program

// Compile command for Windows:
// cl main.c /link framework/redgpu_fdll.lib

// Compile command for Linux:
// cc main.c libredgpu_f.so libglfw.so libboost_filesystem.so.1.71.0 libxml2.so libcurl.so libicudata.so.55 libassimp.so libssl.so libcrypto.so libldap_r-2.4.so.2 liblber-2.4.so.2 libidn.so.11 libgssapi.so.3 libheimntlm.so.0 libkrb5.so.26 libasn1.so.8 libhcrypto.so.4 libroken.so.18 libwind.so.0 libheimbase.so.1 libhx509.so.5 && LD_LIBRARY_PATH=. ./a.out

#include "framework/redgpu_f.h"

RedFHandleFirstPersonCamera * gCamera;

void setup(void) {
  redFSetVerticalSync(0);
  gCamera = redFCreateFirstPersonCamera(1);
  redFFirstPersonCameraControlEnable(gCamera[0]);
}

void draw(void) {
  redFFirstPersonCameraBegin(gCamera[0]);
  redFNoFill();
  redFSetColor(255, 0, 0, 255);
  redFDrawSphere(0, 0, 0, 25.f);
  redFFirstPersonCameraEnd(gCamera[0]);
}

int main() {
  RedFEvents events = {0};
  events.setup = setup;
  events.draw  = draw;
  redFMain(&events, 600, 600, REDF_WINDOW_MODE_WINDOW, 0, 1, 0, 1, 0, 0);
}
```

## Tips:

* Prefer to call `redFDisableArbTex` at setup event to enable normalized texture coordinates and disable the default use of rectangle textures that don't support different texture wrap modes.
* Prefer to call `redFEnableFramebufferSRGBGammaCorrection` at setup event to enable gamma-correct rendering. Images loaded from disk will likely be in the wrong gamma, so convert their 0 to 255 8-bit RGB pixel values to 0.0 to 1.0 float values, apply `r *= r; g *= g; b *= b;` and set the modified float values back to image pixels as 0 to 255 8-bit values.
* REDGPU Framework doesn't call `glfwPollEvents` or `glfwWaitEvents` anywhere internally, so prefer to call `glfwPollEvents` each frame at the beginning of update event to get correct input events.
* Many draw procedures are affected by the currently set `redFSetColor` color.
