#pragma once

#include "ofMain.h"
#include "ofxGifEncoder.h"

class ofxGifRecorder
{
  public:
    ofxGifRecorder();
   ~ofxGifRecorder();

    bool isRecording();
    bool isProcessing();
    void toggleRecording();
    void startRecording();
    void stopRecording();

  private:

    void onWindowResize(ofResizeEventArgs&);
    void onGifSaved(string&);
    void begin(ofEventArgs&);
    void end(ofEventArgs&);

    ofxGifEncoder m_encoder;
    ofFbo m_frame;

    double m_delayTime;
    double m_frameTime;

    bool m_isRec;
    bool m_lockRec;
};
