project(main)
cmake_minimum_required(VERSION 2.8.12)
link_libraries(
  /home/constantine/Documents/redgpu_f/lib-linux/debug-x86-64/libredgpu_f.so
  /home/constantine/Documents/redgpu_f/lib-linux/debug-x86-64/libglfw.so
  -lcurl
  -lm
)
add_executable(main
  main.cpp
)
